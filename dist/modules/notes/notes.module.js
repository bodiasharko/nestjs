"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.NotesModule = void 0;
const common_1 = require("@nestjs/common");
const notes_service_1 = require("./notes.service");
const notes_controller_1 = require("./notes.controller");
const mongoose_1 = require("@nestjs/mongoose");
const note_schema_1 = require("../../schemas/note.schema");
const auth_module_1 = require("../auth/auth.module");
const users_schema_1 = require("../../schemas/users.schema");
const isNoteExist_middleware_1 = require("../../middleware/notesMiddleware/isNoteExist.middleware");
let NotesModule = class NotesModule {
    configure(consumer) {
        consumer
            .apply(isNoteExist_middleware_1.IsNoteExistMiddleware)
            .forRoutes({ path: '/api/notes/:id', method: common_1.RequestMethod.ALL });
    }
};
NotesModule = __decorate([
    (0, common_1.Module)({
        imports: [
            auth_module_1.AuthModule,
            mongoose_1.MongooseModule.forFeature([{ name: note_schema_1.Notes.name, schema: note_schema_1.NotesSchema }]),
            mongoose_1.MongooseModule.forFeature([{ name: users_schema_1.User.name, schema: users_schema_1.UserSchema }]),
        ],
        controllers: [notes_controller_1.NotesController],
        providers: [notes_service_1.NotesService],
    })
], NotesModule);
exports.NotesModule = NotesModule;
//# sourceMappingURL=notes.module.js.map