"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.NotesService = void 0;
const common_1 = require("@nestjs/common");
const mongoose_1 = require("@nestjs/mongoose");
const mongoose_2 = require("mongoose");
const note_schema_1 = require("../../schemas/note.schema");
const users_schema_1 = require("../../schemas/users.schema");
const constants_1 = require("../../constants/constants");
let NotesService = class NotesService {
    constructor(notesModel, userModel) {
        this.notesModel = notesModel;
        this.userModel = userModel;
    }
    async getAllNotes(offset = 0, limit = 0, userId) {
        try {
            const user = await this.userModel.findById(userId);
            const allNotes = [...user.notes];
            let notes = user.notes.splice(offset);
            if (limit) {
                notes = notes.splice(0, limit);
            }
            const fetchedNotes = await Promise.all(notes.map((note) => this.notesModel.findById(note)));
            return {
                offset,
                limit,
                count: allNotes.length,
                notes: fetchedNotes.map((note) => {
                    const { _id, userId, completed, text, createdAt } = note;
                    return { _id, userId, completed, text, createdDate: createdAt };
                }),
            };
        }
        catch (e) {
            throw new common_1.InternalServerErrorException({ message: constants_1.serverError });
        }
    }
    async addNewNote(text, id) {
        try {
            const user = await this.userModel.findById(id);
            const note = await this.notesModel.create({
                text,
                completed: false,
                userId: user._id,
            });
            user.notes.push(note._id);
            await user.save();
            return { message: constants_1.success };
        }
        catch (e) {
            throw new common_1.InternalServerErrorException({ message: constants_1.serverError });
        }
    }
    async getOne(id) {
        try {
            const { _id, userId, completed, text, createdAt } = await this.notesModel.findById(id);
            return {
                note: {
                    _id,
                    userId,
                    completed,
                    text,
                    createdDate: createdAt,
                },
            };
        }
        catch (e) {
            throw new common_1.InternalServerErrorException({ message: constants_1.serverError });
        }
    }
    async updateOne(id, text) {
        try {
            const note = await this.notesModel.findById(id);
            note.text = text;
            await note.save();
            return { message: constants_1.success };
        }
        catch (e) {
            throw new common_1.InternalServerErrorException({ message: constants_1.serverError });
        }
    }
    async checkOne(id) {
        try {
            const note = await this.notesModel.findById(id);
            note.completed = !note.completed;
            await note.save();
            return { message: constants_1.success };
        }
        catch (e) {
            console.log(e);
            throw new common_1.InternalServerErrorException({ message: constants_1.serverError });
        }
    }
    async deleteOne(id, userId) {
        try {
            await this.notesModel.findByIdAndDelete(id);
            await this.userModel.findByIdAndUpdate(userId, {
                $pull: { notes: { $in: [id] } },
            });
            return { message: constants_1.success };
        }
        catch (e) {
            console.log(e);
            throw new common_1.InternalServerErrorException({ message: constants_1.serverError });
        }
    }
};
NotesService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, mongoose_1.InjectModel)(note_schema_1.Notes.name)),
    __param(1, (0, mongoose_1.InjectModel)(users_schema_1.User.name)),
    __metadata("design:paramtypes", [mongoose_2.Model,
        mongoose_2.Model])
], NotesService);
exports.NotesService = NotesService;
//# sourceMappingURL=notes.service.js.map