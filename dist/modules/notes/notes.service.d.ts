import { Model } from 'mongoose';
import { NotesDocument } from '../../schemas/note.schema';
import { User, UserDocument } from '../../schemas/users.schema';
export declare class NotesService {
    private notesModel;
    private userModel;
    constructor(notesModel: Model<NotesDocument>, userModel: Model<UserDocument>);
    getAllNotes(offset: number, limit: number, userId: any): Promise<{
        offset: number;
        limit: number;
        count: number;
        notes: {
            _id: any;
            userId: User;
            completed: boolean;
            text: string;
            createdDate: Date;
        }[];
    }>;
    addNewNote(text: string, id: number): Promise<{
        message: string;
    }>;
    getOne(id: number): Promise<{
        note: {
            _id: any;
            userId: User;
            completed: boolean;
            text: string;
            createdDate: Date;
        };
    }>;
    updateOne(id: number, text: string): Promise<{
        message: string;
    }>;
    checkOne(id: number): Promise<{
        message: string;
    }>;
    deleteOne(id: number, userId: number): Promise<{
        message: string;
    }>;
}
