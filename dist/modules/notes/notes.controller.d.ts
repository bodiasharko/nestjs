import { NotesService } from './notes.service';
import { NotesBodyDto } from '../../dto/notesBody.dto';
import { NotesQueryDto } from '../../dto/notesQuery.dto';
import { NotesParamDto } from '../../dto/notesParam.dto';
export declare class NotesController {
    private readonly notesService;
    constructor(notesService: NotesService);
    getAll({ offset, limit }: NotesQueryDto, req: any): Promise<{
        offset: number;
        limit: number;
        count: number;
        notes: {
            _id: any;
            userId: import("../../schemas/users.schema").User;
            completed: boolean;
            text: string;
            createdDate: Date;
        }[];
    }>;
    addNew({ text }: NotesBodyDto, req: any): Promise<{
        message: string;
    }>;
    get({ id }: NotesParamDto): Promise<{
        note: {
            _id: any;
            userId: import("../../schemas/users.schema").User;
            completed: boolean;
            text: string;
            createdDate: Date;
        };
    }>;
    update({ id }: NotesParamDto, { text }: NotesBodyDto): Promise<{
        message: string;
    }>;
    chek({ id }: NotesParamDto): Promise<{
        message: string;
    }>;
    delete({ id }: NotesParamDto, req: any): Promise<{
        message: string;
    }>;
}
