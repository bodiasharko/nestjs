import { UserDocument } from '../../schemas/users.schema';
import { Model } from 'mongoose';
import { JwtService } from '@nestjs/jwt';
export declare class AuthService {
    private userModel;
    private jwtService;
    constructor(userModel: Model<UserDocument>, jwtService: JwtService);
    register(username: string, password: string): Promise<{
        message: string;
    }>;
    login(username: string): Promise<{
        message: string;
        jwt_token: string;
    }>;
}
