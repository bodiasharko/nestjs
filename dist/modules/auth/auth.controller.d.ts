import { AuthService } from './auth.service';
import { UserDto } from '../../dto/user.dto';
export declare class AuthController {
    private readonly authService;
    constructor(authService: AuthService);
    register({ username, password }: UserDto): Promise<{
        message: string;
    }>;
    login({ username }: UserDto): Promise<{
        message: string;
        jwt_token: string;
    }>;
}
