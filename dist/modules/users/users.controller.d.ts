import { UsersService } from './users.service';
import { UpdatePasswordDto } from '../../dto/updatePassword.dto';
export declare class UsersController {
    private readonly userService;
    constructor(userService: UsersService);
    get(req: any): Promise<{
        user: {
            _id: any;
            username: string;
            createdDate: Date;
        };
    }>;
    update({ newPassword }: UpdatePasswordDto, req: any): Promise<{
        message: string;
    }>;
    delete(req: any): Promise<{
        message: string;
    }>;
}
