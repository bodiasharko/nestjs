import { UserDocument } from '../../schemas/users.schema';
import { Model } from 'mongoose';
import { NotesDocument } from '../../schemas/note.schema';
export declare class UsersService {
    private notesModel;
    private userModel;
    constructor(notesModel: Model<NotesDocument>, userModel: Model<UserDocument>);
    getMe(id: number): Promise<{
        user: {
            _id: any;
            username: string;
            createdDate: Date;
        };
    }>;
    updatePassword(password: string, id: number): Promise<{
        message: string;
    }>;
    delete(id: number): Promise<{
        message: string;
    }>;
}
