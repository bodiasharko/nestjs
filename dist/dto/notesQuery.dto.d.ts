export declare class NotesQueryDto {
    readonly offset: number;
    readonly limit: number;
}
