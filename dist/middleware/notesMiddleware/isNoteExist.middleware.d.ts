import { NestMiddleware } from '@nestjs/common';
import { NotesDocument } from '../../schemas/note.schema';
import { Model } from 'mongoose';
export declare class IsNoteExistMiddleware implements NestMiddleware {
    private notesModel;
    constructor(notesModel: Model<NotesDocument>);
    use(req: any, res: any, next: any): Promise<void>;
}
