import { NestMiddleware } from '@nestjs/common';
export declare class IsUserDataValidMiddleware implements NestMiddleware {
    use(req: any, res: any, next: any): void;
}
