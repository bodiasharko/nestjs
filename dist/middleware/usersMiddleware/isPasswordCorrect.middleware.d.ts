import { NestMiddleware } from '@nestjs/common';
import { UserDocument } from '../../schemas/users.schema';
import { Model } from 'mongoose';
export declare class IsPasswordCorrectMiddleware implements NestMiddleware {
    private userModel;
    constructor(userModel: Model<UserDocument>);
    use(req: any, res: any, next: any): Promise<void>;
}
