"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.IsUserDataValidMiddleware = void 0;
const common_1 = require("@nestjs/common");
const constants_1 = require("../../constants/constants");
class IsUserDataValidMiddleware {
    use(req, res, next) {
        const { username, password } = req.body;
        if (!username || !password) {
            throw new common_1.BadRequestException({ message: constants_1.noData });
        }
        next();
    }
}
exports.IsUserDataValidMiddleware = IsUserDataValidMiddleware;
//# sourceMappingURL=isUserDataValid.middleware.js.map