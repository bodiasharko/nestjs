"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.noNote = exports.matchPassword = exports.unAuth = exports.badPassword = exports.noUser = exports.noData = exports.userExist = exports.clientError = exports.serverError = exports.success = void 0;
exports.success = 'Success';
exports.serverError = 'Internal server error';
exports.clientError = 'This is not your note';
exports.userExist = 'User with this name has already exist';
exports.noData = 'No Username or Password';
exports.noUser = 'No user with this name';
exports.badPassword = 'Incorrect password';
exports.unAuth = 'User not authorized';
exports.matchPassword = 'Old password not match';
exports.noNote = 'Note with this id not exist';
//# sourceMappingURL=constants.js.map