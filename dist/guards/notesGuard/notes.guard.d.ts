import { CanActivate, ExecutionContext } from '@nestjs/common';
import { Model } from 'mongoose';
import { NotesDocument } from '../../schemas/note.schema';
export declare class NotesGuard implements CanActivate {
    private notesModel;
    constructor(notesModel: Model<NotesDocument>);
    canActivate(context: ExecutionContext): Promise<boolean>;
}
