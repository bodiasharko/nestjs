import { CanActivate, ExecutionContext } from '@nestjs/common';
import { UserDocument } from '../../schemas/users.schema';
import { Model } from 'mongoose';
export declare class ChangePasswordGuard implements CanActivate {
    private userModel;
    constructor(userModel: Model<UserDocument>);
    canActivate(context: ExecutionContext): Promise<boolean>;
}
