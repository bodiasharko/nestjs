import { User } from './users.schema';
import * as mongoose from 'mongoose';
export declare type NotesDocument = Notes & mongoose.Document;
export declare class Notes {
    id: number;
    text: string;
    completed: boolean;
    userId: User;
    createdAt: Date;
}
export declare const NotesSchema: mongoose.Schema<mongoose.Document<Notes, any, any>, mongoose.Model<mongoose.Document<Notes, any, any>, any, any, any>, {}>;
