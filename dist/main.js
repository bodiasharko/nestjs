"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const core_1 = require("@nestjs/core");
const app_module_1 = require("./app.module");
async function start() {
    const app = await core_1.NestFactory.create(app_module_1.AppModule);
    app.enableCors();
    await app.listen(Number(process.env.PORT) || 8080);
}
start().then(() => {
    console.log(`Server started on port: ${process.env.PORT || 8080}`);
});
//# sourceMappingURL=main.js.map